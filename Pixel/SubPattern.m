function [C_row, C_col, C_count,NZsubC] = SubPattern(C,bw,fepc )
%SUBPATTERN Summary of this function goes here
% Return an occupation pattern of coarse element,
%using a compressed form, the same size as total non-zero value of C;


[C_col,C_row,~] = find(sparse(C)');
C_count=size(C_row,1);

NZsubC=zeros(fepc,fepc,C_count);

for i= 1:C_count
    x= C_row(i);
    y= C_col(i);
    NZsubC(:,:,i)=bw(1+fepc*(x-1):fepc*x, 1+fepc*(y-1):fepc*y);
end

