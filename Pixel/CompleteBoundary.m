function [ s ] = CompleteBoundary( Csub, fepc )
%found Boundary that does not match the coarse element boundary
s=struct;
N_node=0;
N_dof=0;
i_xplus=0;
i_xminus=0;
i_yplus=0;
i_yminus=0;

for ix=1:fepc
    for iy=1:fepc
        switch ix
            case 1 %ix =1 xminus interface
                if (Csub(ix,iy)==1)
                    i_node= (fepc+1)*(ix-1)+(iy-1);
                    N_node=N_node+1;
                    nodes(N_node)=i_node;
                
                    i_node= (fepc+1)*(ix-1)+iy;
                    N_node=N_node+1;
                    nodes(N_node)=i_node;
                    
                    i_xminus=ixminus+1;
                    xminus(i_xminus)=iy-1;
                    
                    i_xminus=ixminus+1;
                    xminus(i_xminus)=iy;
               
                
                    switch iy 
                        case 1 %ix =1 iy=1
                            i_node= (fepc+1)*(ix)+iy-1;
                            N_node=N_node+1;
                            nodes(N_node)=i_node;
                        case fepc
                            i_node= (fepc+1)*(ix)+iy-1;
                            N_node=N_node+1;
                            nodes(N_node)=i_node;
                        otherwise     
                    end
                end
            case fepc
                 switch iy 
                    case 1
                    case fepc
                    otherwise
                 end
            otherwise
                switch iy 
                    case 1
                    case fepc
                    otherwise
                       if (Csub(ix,iy)==1)&&(Csub(ix+1,iy)==0||Csub(ix-1,iy)==0||Csub(ix,iy-1)==0||Csub(ix,iy+1)==0)
                            s.x(i)=iy;
                            s.y(i)=ix;
                            i=i+1;
                       end
                end

        end
        
    end
end
end


