function [ IsOccupied] = IsOccupied(Ny,Nx,fepc,bw)
%ISOCCUPIED True if some element is valid
IsOccupied= false(Ny,Nx);
for y=1:Ny
    for x=1:Nx
        A=bw(((y-1)*fepc+1):((y)*fepc),((x-1)*fepc+1):((x)*fepc));
        IsOccupied(y,x)=any(any(A));
    end
end
end
