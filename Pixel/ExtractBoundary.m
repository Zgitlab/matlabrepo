function [ s ] = ExtractBoundary( subC,fepc )
%EXTRACTBOUNDARY Summary of this function goes here
%   Detailed explanation goes here
s=struct;
s.xplus=[];
s.xminus=[];
s.yplus=[];
s.yminus=[];

v=(find(subC(:,fepc)));
for i=1:size(v,1)
    s.xplus(i)=(v(i));
end

v=(find(subC(:,1)));
for i=1:size(v,1)
    s.xminus(i)=(v(i));
end


v=(find(subC(1,:)));
for i=1:size(v,2)
    s.yminus(i)=(v(i));
end

v=(find(subC(fepc,:)));
for i=1:size(v,2)
    s.yplus(i)=(v(i));
end 
end

