% x = 6*[0 1 1 2 2 3 3 3 3 2 2 1 1 0 0 0 0];
% y = 6*[0 0 1 1 0 0 1 2 3 3 2 2 3 3 2 1 0];

% 
x = 30*[0   0  1 1  0 ];
y = 30*[0   3  3 0  0 ];
% x = 10*[3   0  4 4   7  7 11 11 7  3];
% y = 10* [0   4  7 10  10 7 7  3  3  0];


x1=x/2;
y1=y/2;
xc=ceil((max(x)-min(x))/2);
yc=ceil((max(y)-min(y))/2);
x1c=ceil((max(x1)-min(x1))/2);
y1c=ceil((max(y1)-min(y1))/2);

fepc=10;
fpc=fepc+1;
Nx=ceil(max(x)/fepc);
Ny=ceil(max(y)/fepc);

ximage=ceil(fepc*Nx/2);
yimage=ceil(fepc*Ny/2);
dx= ximage-xc;
dy= yimage-yc;
dx1= ximage-x1c;
dy1= yimage-y1c;

x=x+dx;
y=y+dy;

x1=x1+dx1;
y1=y1+dy1;

bw=double(poly2mask(x,y,(fepc)*Ny,(fepc)*Nx));
for i=1:size(bw,1)
    for j=1:size(bw,2)
        if bw(i,j)==1
            a=mod(i,4);
            if (a==0)||(a==1)
                bw(i,j)=1;
            else 
                bw(i,j)=3;
            end        
        end
    end
end
%bw1=poly2mask(x1,y1,(fepc)*Ny,(fepc)*Nx);
%bw=bw&(~bw1);
C=IsOccupied(Ny,Nx,fepc,bw);
[C_row, C_col, C_count,NZsubC] = SubPattern(C,bw,fepc);

csvwrite('CoarseElementDistribution.csv',C);
csvwrite('GlobalFineElementDistribution.csv',bw);
for i=1:C_count
    str1= 'sub';
    str2='.csv';
    xc=C_row(i);
    yc=C_col(i);
    out = sprintf('%s%d%s',str1,Nx*(xc-1)+yc-1,str2);
    csvwrite(out,NZsubC(:,:,i));
end

% for i=1:C_count          
%     figure;
%     imshow(NZsubC(:,:,i))
%     
%     a=ExtractBoundary( NZsubC(:,:,i),fepc);
%     
%     y1=ones(size(a.yplus));
%     hold on
%     plot(a.yplus,fepc*y1,'r','LineWidth',1)
%     
%     
%     y1=ones(size(a.yminus));
%     hold on
%     plot(a.yminus,y1,'r','LineWidth',1)
%     
%     
%     x1=ones(size(a.xplus));
%     hold on
%     plot(fepc*x1,a.xplus,'g','LineWidth',1)
%     
%     
%     x1=ones(size(a.xminus));
%     hold on
%     plot(x1,a.xminus,'g','LineWidth',1)
%     hold off;
%     
%     
% end


figure;

imshow(bw)
hold on
plot(x,y,'b','LineWidth',1)
hold off