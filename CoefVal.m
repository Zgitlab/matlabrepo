function [opt] = CoefVal(nij)

arrayW=reshape(nij, [2,2,9,4]);
aW=permute(arrayW, [2,1,3,4]);
inner=load('inner.txt');

opt = 0;
for j1=1:9
    for j2 =1:9
        for i=1:4
            W1=aW(:,:,j1,i);
            W2=aW(:,:,j2,i);
            opt=opt + trace(W1*W2')*inner(j1,j2);                                            
        end
    end
end
