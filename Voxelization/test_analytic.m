      fepc=10;%fine element in coarse element
      nx=3; % number of coarse element in each dimension
      ny=3;
      nz=3;
      
      a = zeros(nx* fepc,ny*fepc, nz*fepc);
      for i=1: nx*fepc; x = i- nx*fepc/2;
        for j=1: ny*fepc; y = j-ny*fepc/2;
          for k=1: nz*fepc; z = k-nz*fepc/2;
            if sum([ x y z ].^2)<=30^2; a(i,j,k) = 1; end
          end
        end
      end
      sp = sparse3D(a);
      
     fpc=fepc+1;
     Nx=fepc*nx+1;
     Ny=fepc*ny+1;
     Nz=fepc*nz+1;
     