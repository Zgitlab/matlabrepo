W= optimvar('W',2,2,4);
PU=sum(W,3)==eye(2,2);

XH=[-1,1,1,-1;
    -1,-1,1,1];

Xh=[-0.5;-0.5];

dX= XH-repmat(Xh,[1,4]);
dXx= dX;
dXx(1,:)=-dX(2,:);
dXx(2,:)=dX(1,:);

a = optimexpr(2,4);
for i=1:4
    a(:,i)=W(:,:,i)*dXx(:,i);
end
RO= sum(a,2)==[0;0];

prob.Constraints.RO = RO;
prob.Constraints.PU = PU;