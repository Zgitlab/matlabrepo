%-------------------Symbolic computation of Stress derivatives------------------
% INPUT : Expression of stress P(F)=..... 
% OUTPUT : function returning dPdF in file dPdF.x
% Antoine CHAN-LOCK
clear all; clc;
% needed using Octave, remove is using matlab
syms mu lambda f1 f2 f3 f4 f5 f6 f7 f8 f9;
F=[f1 f2; f3 f4];
E=(1/2)*(F.'*F-eye(2));
f1=1;
f2=2;
f3=3;
f4=4;
lambda=576.923;
mu=384.615;
W=lambda/2*(trace(E))^2+mu*trace(E*E);
dW=diff(W,f2);
a=subs(dW);
ddW=diff(dW,f2);
b=subs(ddW);
%---------------------------------USER INPUT------------------------------------
% put your stress tensor here
% ref : http://redbkit.github.io/redbKIT/math/SolidMechanics/

P=F*(2*mu*E + lambda * trace(E) * eye(2)); %St Venant Kirchoff model
%P=2*mu*e+lambda*trace(e)*eye(3); %linear

%-------------------------------------------------------------------------------
dP=diff(P,f4);
%can also generate LaTeX, C, Fortran
%see @sym/ccode, @sym/fortran, @sym/latex, @sym/matlabFunction