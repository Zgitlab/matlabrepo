W= optimvar('W',2,2,4);
PU=sum(W,3)==eye(2,2);

DataCoarse=load('DataCoarse.txt');
DataFine=load('DataFine.txt');

XH=[-1,1,1,-1;
    -1,-1,1,1];

Xh=zeros(2,1);
% dX=zeros(2,4);
% 
% for i=1:4
%     dX(:,i)= XH(:,i)-Xh(:);
% end
% 
% a=-dX(2,:);
% dX(2,:)=dX(1,:);
% dX(1,:)=a;
% dX=15*dX;

dX(:,1)=[30;-13];
dX(:,4)=[0;-13];
dX(:,2)=[30; 17];
dX(:,3)=[0 ; 17];



a = optimexpr(2,4);
for i=1:4
    a(:,i)=W(:,:,i)*dX(:,i);
end

RO= sum(a,2)==[0;0];
prob = optimproblem;
prob.Constraints.RO = RO;
prob.Constraints.PU = PU;

n_Data=10;

CoarseData=zeros(2,4,n_Data);
FineData= DataFine';
for i=1:10
    CoarseData(:,:,i)=reshape(DataCoarse(i,:),2,4);
end

b=optimexpr(1);
for j=1:n_Data
    a=optimexpr(2);
    for i=1:4 
        a=a+W(:,:,i)*CoarseData(:,i,j);
    end
    b=b+sum((a-FineData(:,j)).^2);
end
prob.Objective=b;

sol=solve(prob);

